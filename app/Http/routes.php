<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return 'Laravel';
});

//Route::get('user','UserController@index');
//
//Route::get('user/create','UserController@create');
//
//Route::post('user/store','UserController@store');
//
//Route::get('user/{id}/edit','UserController@edit');

Route::resource('user','UserController');
