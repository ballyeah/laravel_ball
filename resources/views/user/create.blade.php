@extends('welcome')

@section('content')
    <h3>สร้างผู้ใช้งาน</h3>
    <hr>
    {!! Form::open(array('action'=>'UserController@store')) !!}
    <div class="form-group">
        {!! Form::label('name','ชื่อผู้ใช้งาน : ') !!}
        {!! Form::text('name','',['placeholder'=>'กรอกชื่อผู้ใช้งาน','class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email','อีเมล์ : ') !!}
        {!! Form::email('email','', array('placeholder'=>'กรอกอีเมล์', 'class'=>'form-control' ) ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password','รหัสผ่าน : ') !!}
        {!! Form::password('password', array('placeholder'=>'กรอกรหัสผ่านผู้ใช้งาน', 'class'=>'form-control' ) ) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('สมัครสมาชิก',['class'=>'btn btn-primary form-control']) !!}
    </div>

    {!! Form::close() !!}

    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                    <p>{{$error}}</p><br>
            @endforeach
        </ul>
    @endif
@stop