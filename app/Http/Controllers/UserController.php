<?php

namespace App\Http\Controllers;

use App\User;
use App\UsersModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::all();

        return view('user.index',['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Requests\CreateUserRequest $userRequest)
    {
        //

        $inputRequest = $request;

        $users = new User();
        $users->name = $inputRequest->name;
        $users->email = $inputRequest->email;
        $users->password = $inputRequest->password;
        $users->created_at = Carbon::now();

        $users->save($userRequest->all());

        return redirect('user');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $users = User::findOrFail($id);

        return view('user.edit',['users'=>$users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,Requests\CreateUserRequest $userRequest)
    {
        //
        $users = User::findOrFail($id);

        $users->name = $request->name;
        $users->email = $request->email;
        $users->updated_at = Carbon::now();
        $users->logged_at = '0000-00-00 00:00:00';
        $users->update($userRequest->all());

        return redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
