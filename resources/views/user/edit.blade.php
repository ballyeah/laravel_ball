@extends('welcome')

@section('content')
    <h3>แก้ไขสมาชิก :: {{ $users->name  }}</h3>
    <hr>
    {!! Form::model($users,['method'=>'PATCH','action'=>['UserController@update',$users->id]]) !!}
    <div class="form-group">
        {!! Form::label('name','ชื่อ : ') !!}
        {!! Form::text('name',null,['placeholder'=>'กรอกชื่อผู้ใช้งาน','class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email','อีเมล์ : ') !!}
        {!! Form::email('email',null, array('placeholder'=>'กรอกอีเมล์', 'class'=>'form-control' ) ) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('แก้ไขสมัครสมาชิก',['class'=>'btn btn-primary form-control']) !!}
    </div>

    {!! Form::close() !!}

    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{$error}}</p><br>
            @endforeach
        </ul>
    @endif
@stop()