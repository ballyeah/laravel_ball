@extends('welcome')

@section('content')
    <h3>จัดการสมาชิก</h3>
    <hr>
    <table class="table table-bordered">
        <tr>
            <td>อีเมล์</td>
            <td>วันที่สมัคร</td>
            <td>เข้าใช้งานล่าสุด</td>
            <td></td>
        </tr>
        @foreach($users as $user)
            <tr>
                <td>{{$user->email}}</td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->logged_at}}</td>
                <td><a href="{{action('UserController@edit',$user->id)}}">แก้ไข</a> | <a href="{{action('UserController@edit',$user->id)}}">ลบ</a></td>
            </tr>
        @endforeach
    </table>
@stop